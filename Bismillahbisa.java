import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

public class Bismillahbisa  extends JFrame {
    private JTable table;
    private DefaultTableModel tableModel;
    private JTextField nimField;
    private JTextField namaField;
    private JTextField no_absenField;
    private JTextField program_studiField;

    public Bismillahbisa() {
        setTitle("Database GUI");
        setSize(600, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        tableModel = new DefaultTableModel(new String[]{"nim", "nama", "no_absen", "program_studi"}, 0);
        table = new JTable(tableModel);
        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane, BorderLayout.CENTER);

        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new GridLayout(5, 2, 10, 10));
        inputPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        inputPanel.add(new JLabel("Nim:"));
        nimField = new JTextField();
        inputPanel.add(nimField);
        inputPanel.add(new JLabel("Nama:"));
        namaField = new JTextField();
        inputPanel.add(namaField);
        inputPanel.add(new JLabel("No_Absen:"));
        no_absenField = new JTextField();
        inputPanel.add(no_absenField);
        inputPanel.add(new JLabel("Program_studi:"));
        program_studiField = new JTextField();
        inputPanel.add(program_studiField);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));

        JButton addButton = new JButton("Add");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addData();
            }
        });
        buttonPanel.add(addButton);

        JButton updateButton = new JButton("Update");
        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateData();
            }
        });
        buttonPanel.add(updateButton);

        JButton deleteButton = new JButton("Delete");
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteData();
            }
        });
        buttonPanel.add(deleteButton);

        JPanel southPanel = new JPanel();
        southPanel.setLayout(new BorderLayout());
        southPanel.add(inputPanel, BorderLayout.CENTER);
        southPanel.add(buttonPanel, BorderLayout.SOUTH);

        add(southPanel, BorderLayout.SOUTH);

        loadData();
    }

    private void loadData() {
        try (Connection conn = Cobaaja.getConnection();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM biodata")) {

            while (rs.next()) {
                String nim = rs.getString("nim");
                String nama = rs.getString("nama");
                String no_absen = rs.getString("no_absen");
                String program_studi = rs.getString("program_studi");
                tableModel.addRow(new Object[]{nim, nama, no_absen, program_studi});
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addData() {
        String nim = nimField.getText();
        String nama = namaField.getText();
        String no_absen = no_absenField.getText();
        String program_studi = program_studiField.getText();

        try (Connection conn = Cobaaja.getConnection();
             PreparedStatement pstmt = conn.prepareStatement("INSERT INTO biodata (nim, nama, no_absen, program_studi) VALUES (?, ?, ?, ?)")) {

            pstmt.setString(1, nim);
            pstmt.setString(2, nama);
            pstmt.setString(3, no_absen);
            pstmt.setString(4, program_studi);
            pstmt.executeUpdate();

            tableModel.addRow(new Object[]{nim, nama, no_absen, program_studi});
            nimField.setText("");
            namaField.setText("");
            no_absenField.setText("");
            program_studiField.setText("");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void updateData() {
        int selectedRow = table.getSelectedRow();
        if (selectedRow != -1) {
            String nim = nimField.getText();
            String nama = namaField.getText();
            String no_absen = no_absenField.getText();
            String program_studi = program_studiField.getText();

            try (Connection conn = Cobaaja.getConnection();
                 PreparedStatement pstmt = conn.prepareStatement("UPDATE biodata SET nama = ?, no_absen = ?, program_studi = ? WHERE nim = ?")) {

                pstmt.setString(1, nama);
                pstmt.setString(2, no_absen);
                pstmt.setString(3, program_studi);
                pstmt.setString(4, nim);
                pstmt.executeUpdate();

                tableModel.setValueAt(nim, selectedRow, 0);
                tableModel.setValueAt(nama, selectedRow, 1);
                tableModel.setValueAt(no_absen, selectedRow, 2);
                tableModel.setValueAt(program_studi, selectedRow, 3);
                nimField.setText("");
                namaField.setText("");
                no_absenField.setText("");
                program_studiField.setText("");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Select a row to update");
        }
    }

    private void deleteData() {
        int selectedRow = table.getSelectedRow();
        if (selectedRow != -1) {
            String nim = (String) tableModel.getValueAt(selectedRow, 0);

            try (Connection conn = Cobaaja.getConnection();
                 PreparedStatement pstmt = conn.prepareStatement("DELETE FROM biodata WHERE nim = ?")) {

                pstmt.setString(1, nim);
                pstmt.executeUpdate();

                tableModel.removeRow(selectedRow);
                nimField.setText("");
                namaField.setText("");
                no_absenField.setText("");
                program_studiField.setText("");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Select a row to delete");
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Bismillahbisa().setVisible(true);
            }
        });
    }
}
